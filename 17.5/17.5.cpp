﻿#include <iostream>
#include <math.h>
using namespace std;

class Something
{
private:

	int a;
	int b;

public:

	void SetPar(int _a, int _b)
	{
		a = _a;
		b = _b;
	}
	void PrintPar()
	{
		cout << a << ' ' << b << endl;
	}
};

class Vector
{
private:

	double x, y, z;

public:

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	double Module()
	{
		double M = sqrt(x * x + y * y + z * z);
		return M;
	}

};


int main()
{

	Something temp;
	temp.SetPar(4, 6);
	temp.PrintPar();

	Vector point(5, 5, 5);

	cout << point.Module();

}